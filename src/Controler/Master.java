/**
 * Package Controler: Contains all the classes for clustering keywords
 */
package Controler;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import keywordClustering.KeywordGraph;

/**The Master Class: Contains the main method and acts as the front end.
 * 
 * @author Rajat Raychaudhuri
 */
public class Master {
	
	/** The Connection object to connect the database: presently not used*/
	private static Connection conn = null;
	
	/** The Statement object for the database: presently not used*/
	private static Statement stmt = null; 
	
	/** The ResultSet object for the database: presently not used*/
    private static ResultSet rs = null;
    
    /** Date object to record the start time*/
    private static Date startTime;
    
    /** Date object to record the end time*/
    private static Date endTime;
    
    /** Name of the input raw file*/
    private static String dataFileName;
    
    /** Name of the graph file*/
    private static String graphFileName;
    
    /** Delimiter for the dataFileName*/
    private static String delm;
    
    /** Format for the Dates */
    private static SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
    
    private static KeywordGraph myGraph;
	
    /**The main method : 
     * 
     * @param args	Command line arguments : number of partitions(int);weight threshold(int);
     */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		
		if(args.length < 7){
			System.out.println("Length:"+args.length);
			System.out.println("Improper Number of Arguments !!!");
			System.out.println("Usage:\targs[0]=datafile\n\targs[1]=graphfile\n\targs[2]=number of partitions");
			System.out.println("Optional:\targs[3]=Minimum weight\n\targs[4]=wait\n\targs[5]=Delimiter");
			System.exit(1);
		}
		
		System.out.println("Length:"+args.length);
		
		dataFileName = args[0];
		graphFileName = args[1];
		int partition = Integer.parseInt(args[2]);
		int minWeight = args.length >= 4 ? Integer.parseInt(args[3]) : 1;
		int wait = args.length >= 4 ? Integer.parseInt(args[4]) : 15;
		delm = args.length == 6 ? args[5] : null;
		String otherfile = args[6];
		
		//Print the Start Time
		startTime = new java.util.Date();
 		System.out.println("\nStart time:" + formatter.format(startTime) + "\n");
 		
 		if(delm != null){
 			dataFileName = getOnlyKeywords(dataFileName,delm);
 		}
 		
 		/*
		String dataFileName = "/Users/rajatraychaudhuri/My_Programs/fall_2010/algo/mydata";
		String graphFileName = "/Users/rajatraychaudhuri/My_Programs/fall_2010/algo/mygraph";
		String dataFileName = new String("/home/rajat/current/dataFile");
		String graphFileName = new String("/home/rajat/current/graphFile");
		populateDataFile(dataFileName);
		*/
 		
		myGraph = new KeywordGraph(dataFileName,graphFileName,1,minWeight,partition);
	
		System.out.println("1. Calling the buildGraph function:");
		myGraph.buildGraph();
		System.out.println("\n2. Write the graphfile for partitioning");
		myGraph.writeGraphFile();
		myGraph.AnalyzeGraph();
		System.out.println("3. Partition the graph file into "+ partition + " components");
	
		
		String partitionedFile = partitionGraphFile(graphFileName,partition);
		
			try{
			  //do what you want to do before sleeping
			  Thread.currentThread().sleep(1000*wait);//sleep for 5000 ms
			  //do what you want to do after sleeptig
			}
			catch(InterruptedException ie){
			//If this thread was intrrupted by nother thread 
			}
		
		
		System.out.println("4. Assigning the keywords to clusters");
		myGraph.assignClustersToKeywords(partitionedFile);
		
		getAffinity(otherfile);
		
		System.out.println("\nALL OPERATION COMPLETED SUCCESSFULLY...BYE");
		//Print the End Time
 		endTime = new java.util.Date();
 		System.out.println("\nEnd time:" + formatter.format(endTime));

	}
	
	/**
	 * A static method to partition a graph using metic
	 * @param graphFileName		Name of the Metis input file
	 * @param partition			Number of partitions
	 * @return 					The name of the file where meris stores all the partition info
	 */
	public static String partitionGraphFile(String graphFileName,int partition ){
		
		Runtime rt = Runtime.getRuntime();
	     //p = null;
	    
	    
	    	try
	       {
	    		Process p = rt.exec( "kmetis " + graphFileName + " " + partition);
	       }
	       catch ( IOException ioe )
	       {
	           System.out.println( "Error partitioning graph file" );
	       }
	       
	       //InputStream output = p.getInputStream();
	       //System.out.println( output );
	     
	    String filename = graphFileName + ".part." + partition;   
		return filename;
		
	}
	
	
	/** 
	 * Public Static method to populate the raw input file from the database : Presently not used
	 * 
	 * @param dataFileName		A filename to store the data
	 */
	public static void populateDataFile(String dataFileName){
		
		FileOutputStream dataFile = null;
		//String sql = new String("Select keywords from youtube_video limit 1,1000");
		String sql = new String("Select keywords from youtube_video");
		String temp = null;
		
		//errorCount = 0;
		
		try{
			dataFile = new FileOutputStream(dataFileName);
		}catch(IOException e){
			System.out.println("Data File Opening Failed !!!!");
			System.exit(1);
		}
		
		setConnection();
		
		try{
			stmt = conn.createStatement(); 
			rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				try{
					temp = rs.getString("keywords") +"\n" ;
					dataFile.write(temp.getBytes());
					
				}catch(IOException e){
					System.out.println("Some Error occured while data write");
					continue;
				}catch(Exception e){continue;}
				
			}
		
		}catch(SQLException e){
			System.out.println("Some Error occured while data fetch");
		}
		
		closeConnection();
		
		try{
			dataFile.close();
		}catch(IOException e){
			System.out.println("Problem while closing Data file!!!!");
		}
		
	}

	/**  Public Static method to connect the database : Presently not used*/
	public static void setConnection(){
    	try
	    {
	      Class.forName("com.mysql.jdbc.Driver").newInstance();
	      String url = "jdbc:mysql://matrix.cs.fsu.edu:3306/SmartPlayer?";
	      conn = DriverManager.getConnection(url + "user=smartplayer&password=smartplayer123");

	      System.out.println("Connected.....\n");
	    }catch (ClassNotFoundException ex) {System.err.println(ex.getMessage());}
	    catch (IllegalAccessException ex) {System.err.println(ex.getMessage());}
	    catch (InstantiationException ex) {System.err.println(ex.getMessage());}
	    catch (SQLException ex)           {System.err.println(ex.getMessage());}
    }
    
	/**  Public Static method to close the connection with the database : Presently not used*/
    public static void closeConnection(){
    	
    	try{
    		conn.close();
		  System.out.println("Connection closed");
    	}catch (SQLException ex){
    		 
			System.out.println("SQLException: " + ex.getMessage()); 
			System.out.println("SQLState: " + ex.getSQLState()); 
			System.out.println("VendorError: " + ex.getErrorCode());
	} finally {

			if (rs != null) {
				try { 
						rs.close();
				}catch (SQLException sqlEx) { } // ignore
			}  

			if (stmt != null) {
	
				try { 
						stmt.close();
				} catch (SQLException sqlEx) { } // ignore
			}
	}
   }
    
    /**
     * A static method to first separate the words from the raw file
     * @param sourcefile	Name of the source file (raw file)
     * @param delm			Delimiter for the file separating item id and tags
     * @return				The name of the file containing only keywords
     */
    public static String getOnlyKeywords(String sourcefile,String delm){
		
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		
		FileOutputStream df = null;
		String outfile = sourcefile + "_wordsOnly";
		
		String Line = null;
		String[] words = null;
		String w = "";
		
		try{
			fstream = new FileInputStream(sourcefile);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			df = new FileOutputStream(outfile);
		}catch(FileNotFoundException e){
			System.out.println("Datafile could not be opened");
			System.exit(1);
		}
		
		try{
			while ((Line = br.readLine()) != null){
				
				Line.replaceAll("\\W","");
				words = Line.split(delm);
				
				w = "";
				
				for(int i=1;i<words.length;i++)
					w = w + words[i];
				
				w = w + "\n";
					
				df.write(w.getBytes());
				
				
			}
		
			}catch(IOException e){
				System.out.println("Problem in IO");
			}
			
			try{
				fstream.close();
				in.close();
				br.close();
				df.close();
			}catch(IOException e){
				System.out.println("Problem while closing the Datafile");
				//System.exit(1);
			}
			
			return outfile;
		
	}
    /**
     * A static method to get the related keywords from a videoid
     * @param videoid	The videoid 
     * @return			A String containing all the keywords separated with commas 
     */
    public static String getKeywords(String videoid){
    	
    	
		String s =null;
		Process p = null;
		BufferedReader stdInput;
		String crap = null;
		
		Runtime rt = Runtime.getRuntime();
		
		/*
		try
	       {
				s = "grep " + videoid + " " + dataFileName + " | awk '{print substr($0,15)}'";
				p = rt.exec(s);
				stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
				
				while ((s = stdInput.readLine()) != null) 	
						s = s.split(":")[1].split(delm)[1];
			
						
				
	       }
	       catch ( IOException ioe )
	       {
	           System.out.println( "Error partitioning graph file" );
	           return null;
	       }
	       */
		
		try
	       {
				s = "grep " + videoid + " " + dataFileName + " | awk '{print substr($0,15)}'";
				p = rt.exec(s );
				stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while ((s = stdInput.readLine()) != null) {
					
						 crap = s.split(":")[1].split("#@#")[1];
						//String[] bullcrap = crap[1].split("#@#");
					   //System.out.println(crap);
				}
	       }
	       catch ( IOException ioe )
	       {
	           System.out.println( "Error partitioning graph file" );
	       }
		
	       return crap;
	      
    }
    
   /**
    * The method for guessing the ratings depending on history 
    * @param filename	The file containing the videoids that an user liked, did not like and unknown 
    */
    public static void getAffinity(String filename){
    	
    	FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		
		int count = 1;
		
		String Line = null;
		String[] words = null;
		String keywords = null;
		double[] aff = null;
		
		
		Queue<String> good = new LinkedList<String>();
		Queue<String> bad = new LinkedList<String>();
		Queue<String> ugly = new LinkedList<String>();
		
		try{
			
			fstream = new FileInputStream(filename);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			
		}catch(FileNotFoundException e){
			System.out.println("Datafile could not be opened");
			System.exit(1);
		}
		
		
			try {
				while ((Line = br.readLine()) != null){
					
					Line.replaceAll("\\W","");
					words = Line.split(",");
					
					for(int i = 0;i<words.length;i++){
						
						switch(count){
						
							case 1: good.add(words[i].trim());
									break;
							case 2: bad.add(words[i].trim());
									break;
							case 3: ugly.add(words[i].trim());
									break;
							default: System.out.println("WRONG !!!!");
									break;
						}
					}
					
					count++;
					
				}
				
				fstream.close();
				in.close();
				br.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		System.out.println("The Good Videos::");
		try{
			while(true){
				
				Line = good.remove().trim();
				keywords = getKeywords(Line);
				
				if(keywords == null)
					continue;
					
				words = keywords.split(",");
				aff = myGraph.getAffinityVector(words);
				System.out.print(Line);
				
				for(int i=0;i<aff.length;i++){
					System.out.print("\t" + aff[i]);
				}
				
				System.out.print("\n");
				
			}
		}catch(NoSuchElementException x){
			System.out.println("\n\n");
		}
			
			
		System.out.println("The Bad Videos::");
		try{
			while(true){
				
				Line = bad.remove().trim();
				keywords = getKeywords(Line);
				
				if(keywords == null)
					continue;
				
				words = keywords.split(",");
				aff = myGraph.getAffinityVector(words);
				System.out.print(Line);
				
				for(int i=0;i<aff.length;i++){
					System.out.print("\t" + aff[i]);
				}
				
				System.out.print("\n");
				
			}
		}catch(NoSuchElementException x){
			System.out.println("\n\n");
		}
			
    }

}
