/**
 * Package keywordClustering: Contains all the classes for clustering keywords
 */
package keywordClustering;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;

/**The KeywordGraph Class: Stores the keyword graph and provides various utility methods
 * 
 * @author Rajat Raychaudhuri
 */
public class KeywordGraph {
	
	/**	The raw input filename (with absolute path)	*/
	private String dataFileName;
	
	/**	The generated graph filename (with absolute path); This file is fed to Metis for partitioning*/
	private String graphFileName;
	
	/**	A threshold on occurrence of the keywords in the input file : presently not used; default value: 1*/
	private int minOccuranceThreshold;
	
	/**	Sum total of all the frequency of all the keywords*/
	private int totalOccurance = 0;
	
	/**	A threshold on weight of the keywords in the input file : set by the user as a command line argument; default value: 1*/
	private int minWeightThreshold;
	
	/** Number of vertices (keywords) in the graph*/
	private int vertexCount;
	
	/** Number of edges (links between keywords) in the graph*/
	private int edgeCount;
	
	/** Not used presently*/
	private int errorCount;
	
	/** Number of Partitions */
	private int components;
	
	/** Number of edges removed due to lack of weight*/
	private int edgeRemoved;
	
	/** Number of idolated keywords removed */
	private int keywordRemoved;
	
	/** Pool of all keywords used in the graph*/
	private BidiMap<Keyword,Integer> keywords ;
	
	/** Pool of all edges used in the graph*/
	private HashMap<String,Edge> edges;
	
	/** 
	 * Creates a KeywordGraph object with the specified parameters; calls KeywordGraph(String dataFile,String graphFile,int minOccur,int minWeight)
	 * 
	 * @param dataFile			The word
	 * @param graphFile			Degree of the keyword in the keyword graph
	 */
	public KeywordGraph(String dataFile,String graphFile){
		this(dataFile,graphFile,1,1,5);
	}
	
	/** 
	 * Creates a KeywordGraph object with the specified parameters; calls KeywordGraph(String dataFile,String graphFile,int minOccur,int minWeight)
	 * 
	 * @param dataFile			The word
	 * @param graphFile			Degree of the keyword in the keyword graph
	 * @param minweight			The minWeightThreshold for the edges
	 */
	public KeywordGraph(String dataFile,String graphFile,int minweight){
		this(dataFile,graphFile,1,minweight,5);
	}
	
	/** 
	 * Creates a KeywordGraph object with the specified parameters; calls KeywordGraph(String dataFile,String graphFile,int minOccur,int minWeight)
	 * 
	 * @param dataFile			The word
	 * @param graphFile			Degree of the keyword in the keyword graph
	 * @param minOccur			A threshold on occurrence of the keywords in the input file : presently not used; default value: 1
	 * @param minWeight			A threshold on weight of the keywords in the input file : set by the user as a command line argument; default value: 1
	 * @param parts				Number of partitions : default 5
	 */
	public KeywordGraph(String dataFile,String graphFile,int minOccur,int minWeight,int parts){
		dataFileName = dataFile;
		graphFileName = graphFile;
		
		minOccuranceThreshold = minOccur;
		minWeightThreshold = minWeight;
		
		keywords = new DualHashBidiMap<Keyword,Integer>();
		edges = new HashMap<String,Edge>();
		
		components = parts;
		
	}	

	
	/**
	 * A public instance method to show the vertex count and edge count of the graph
	 */
	public void AnalyzeGraph(){
		System.out.println("\n\n");
		System.out.println("####################################################################");
		System.out.println("                          Analyzing Graph                           ");
		System.out.println("####################################################################");
		
		System.out.println("Total keywords:"+vertexCount+"\t\t\tTotal edges:"+edgeCount);
		System.out.println("Edges removed during compaction:" + edgeRemoved + "\tKeywords removed during compaction:"+ keywordRemoved);
		System.out.println("\n\n");
	}
	
	
	public int getTotalOccurance(){
		return totalOccurance;
	}
	
	/**
	 * A public instance method to build the graph the given input file
	 */
	public void buildGraph(){
		
		System.out.println("\ta. Populating Keywords....");
		populateKeywords();
		
		System.out.println("\tb. Populating Edges....");
		populateEdges();
		
		System.out.println("\tc. Removing Insignificant Edges....");
		removeInsignificantEgdes();
		
		System.out.println("\td. Removing Isolated Keywords....");
		removeIsolatedKeywords();
		
	}
	
	/**
	 * An instance method with restricted access(package) to populate the keyword pool from the input file
	 */
	void populateKeywords(){
		
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		
		String Line = null;
		String[] words = null;
		String word = null;
		Keyword k;
		
		errorCount = 0;
		
		try{
			fstream = new FileInputStream(dataFileName);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
		}catch(FileNotFoundException e){
			System.out.println("Datafile could not be opened");
			System.exit(1);
		}
		
		try{
				while ((Line = br.readLine()) != null){
					
					Line.replaceAll("\\W","");
					words = Line.split(",");
						
						
					for(int i=0;i<words.length;i++){
						try{
							word = Keyword.purify(words[i]);
							k = new Keyword(word,0);
							if(word != "" && keywords.containsKey(k) == false){
								addKeyword(word);
							}else if (keywords.containsKey(k) == true){
								int r = keywords.get(k);
								k = keywords.getKey(r);
								k.increaseOccurrence();

							}
								
						}catch(Exception e){
							errorCount++;
							continue;
						}
					}
				}
				
			}catch(IOException e){
				System.out.println("Problem in IO");
			}
			
			keywords.remove("");
			keywords.remove("\\W");
			
	try{
			fstream.close();
			in.close();
			br.close();
		}catch(IOException e){
			System.out.println("Problem while closing the Datafile");
			//System.exit(1);
		}
	}
	
	/**
	 * An instance method with restricted access(package) to populate the edge pool from the input file
	 */
	void populateEdges(){
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		
		String Line = null;
		String[] words = null;
		String w1,w2;
		
		int i1 = -1;
		int i2 = -1;
		
		Keyword k1 = null;
		Keyword k2 = null;
		Edge e = null;
		
		String hashCode;
		errorCount = 0;
		
		
		try{
			fstream = new FileInputStream(dataFileName);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
		}catch(FileNotFoundException x){
			System.out.println("Datafile could not be opened");
			System.exit(1);
		}
		
		try{
			while ((Line = br.readLine()) != null){
					
					
				Line.replaceAll("\\W","");
				words = Line.split(",");
				
				/*
				if(edgeCount%1000 == 0){
					System.out.println("Current edgecount:" + edgeCount);
				}
				*/
					
				for(int i=0;i<(words.length-2);i++){
						
						try{
							w1 = Keyword.purify(words[i]);

							if(w1 == "")
								continue;
							
							k1 = new Keyword(w1,0);
							i1 = keywords.containsKey(k1) ? keywords.get(k1) : -1;
					
							for(int j = i+1; j <(words.length-1);j++ ){
						 
								w2 = Keyword.purify(words[j]);
							
								if(w2 == "")
									continue;
								
								k2 = new Keyword(w2,0);
								i2 = keywords.containsKey(k2) ? keywords.get(k2) : -1;
								
								if(i1 == -1 || i2 == -1)
									continue;
								
								hashCode = i1 > i2 ? Edge.generateHashCode(w2, w1) : Edge.generateHashCode(w1, w2);
								
								if (hashCode == null)
									continue;
								
								if(edges.containsKey(hashCode)){
									
									e = (Edge)edges.get(hashCode);
									e.increaseWeight();
									
								}else{
									
									if(i1 > i2) 
										addEdge(w2,w1);
									else
										addEdge(w1,w2);
								}
								
								
								
							}
						}catch(Exception x){
							errorCount++;
							continue;
						}
					
				}	
				
			}
		}catch(IOException x){
				System.out.println("Problem in IO");
		}
		
		try{
			fstream.close();
			in.close();
			br.close();
		}catch(IOException x){
			System.out.println("Problem while closing the Datafile");
			//System.exit(1);
		}
	}
	
	/**
	 * An instance method with restricted access(package) to remove edges from the edge pool with weights less than minWeightThreshold
	 */
	void removeInsignificantEgdes(){
		
		String[] toBeRemoved = new String[edges.size()];
		Collection<Edge> c = edges.values();
		Iterator<Edge> itr = c.iterator();
		Edge e = null;
		int removeCount = 0;
		
		
		while(itr.hasNext()){
			
			e = (Edge)itr.next();
			if(e.getWeight() < minWeightThreshold){
				toBeRemoved[removeCount++] = e.getHashCode();
			}
				
		}
		
		//System.out.println("Total " + removeCount + " edges removed");
		edgeRemoved = removeCount;
		
		for(int i=0;i<removeCount;i++){
			removeEdge(toBeRemoved[i]);
		}
		
	}

	/**
	 * An instance method with restricted access(package) to remove keywords from the keyword pool with degree = 0
	 */
	void removeIsolatedKeywords(){
		
		int adjustment = 0;
		Keyword k = null;
		int temp;
		
		
		
		for(int i=0;i<=vertexCount;i++){
			
			if(!keywords.containsValue(i))
				continue;
			
			k = keywords.getKey(i);
			temp = k.getDegree();
			keywords.remove(k);
			
			if(temp <= 0){
				
				adjustment++;
				//System.out.println("Keyword:" + k.getWord() + "\tDegree:" + k.getDegree());
				
			}else{
				
				temp = k.getIndex();
				temp -= adjustment;
				k.setIndex(temp);
				
				keywords.put(k, temp);
				totalOccurance += k.getOccurrence();
			
			}
			
		}
		keywordRemoved = adjustment;
		vertexCount -= adjustment;
		//System.out.println("Total "+ adjustment + " keywords removed");
		
	}
	
	/**
	 * An instance method with restricted access(package) for internal testing during development of this code
	 */
	void testFormation(){
		
		Keyword k =null;
		
		for(int i=0;i<=keywords.size();i++){
			if(!keywords.containsValue(i)){
				System.out.println(i +"th keyword missing");
				continue;
			}
			k = keywords.getKey(i);
			System.out.println(k.getIndex()+"." + k.getWord()+"\tDegree:"+k.getDegree());
			
		}
	}
	
	/**
	 * An instance method with restricted access(package) to add a single edge in the edge pool
	 */
	void addEdge(String v1,String v2){
		
		Edge e = new Edge(v1,v2);
		edges.put(Edge.generateHashCode(v1, v2), e);
		edgeCount++;
		
		Keyword k1 = new Keyword(v1,0);
		Keyword k2 = new Keyword(v2,0);
		int temp = 0;
		
		
		temp = keywords.get(k1);
		k1 = keywords.getKey(temp);
		
		
		temp = keywords.get(k2);
		k2 = keywords.getKey(temp);
		
		//temp = e.getWeight();
		k1.increaseDegree();
		k2.increaseDegree();
		
	}
	
	/**
	 * An instance method with restricted access(package) to remove a single edge in the edge pool
	 */
	void removeEdge(String edgehashcode){
		
		Edge e = edges.get(edgehashcode);
		int temp = 0;
		
		String w1 = e.getStartingWord();
		Keyword k1 = new Keyword(w1,0);
		temp = keywords.get(k1);
		k1 = keywords.getKey(temp);
		
		String w2 = e.getStartingWord();
		Keyword k2 = new Keyword(w2,0);
		temp = keywords.get(k2);
		k2 = keywords.getKey(temp);
		
		//temp = e.getWeight();
		k1.decreaseDegree();
		k2.decreaseDegree();
		
		edges.remove(edgehashcode);
		edgeCount--;
		
	}
	
	/**
	 * An instance method with restricted access(package) to add a single keyword in the keyword pool
	 */
	void addKeyword(String w){
		Keyword k = new Keyword(w,++vertexCount);
		keywords.put(k, vertexCount);
	}
	
	
	/**
	 * An instance method to write the graph file which is to be partitioned
	 */
	public void writeGraphFile(){
		
		FileOutputStream graphFile = null;
		String[] list = new String[keywords.size()+1];
		
		for(int i=0;i<list.length;i++)
			list[i] = "";
		
		String w = null;
		String w1,w2;
		Edge e = null;
		
		Keyword k = null;
		int t1 = 0;
		int t2 = 0;
		int t =0 ;
		
		int edge = 0;
		
		Object[] chk = edges.values().toArray();
		
		for(int i=0;i<chk.length;i++){
			
			e = (Edge)chk[i];
			
			w1 = e.getStartingWord();
			k = new Keyword(w1,0);
			if(!keywords.containsKey(k)){
				//System.out.println(w1 + " not found");
				continue;
			}
			t1 = keywords.get(k);
			
			
			w2 = e.getEndingWord();
			k = new Keyword(w2,0);
			
			if(!keywords.containsKey(k)){
				//System.out.println(w2 + " not found");
				continue;
			}
			
			t2 = keywords.get(k);
			
			if (t1 == t2)
				continue;
			
			t = e.getWeight();
			
			try
			{
			list[t1] = list[t1] + t2 + " " + t + " ";
			list[t2] = list[t2] + t1 + " " + t + " ";
			edge++;
			}catch(Exception x){
				System.out.println("Indices:" + t1 + "," + t2 + "\tBound:" + keywords.size());
			}
		}
		
		//System.out.println("Total Edge:" + edge);
		edgeCount = edge;
		vertexCount = keywords.size();
		
		try{
			graphFile = new FileOutputStream(graphFileName);
		}catch(IOException x){
			System.out.println("File Opening Failed !!!!");
			System.exit(1);
		}
		
		w1 = Integer.toString(keywords.size());
		w2 = Integer.toString(edge);
		w = w1 + " " + w2 + " " + "1\n";
		
		try{
			graphFile.write(w.getBytes());
		}catch(IOException x){
			System.out.println("Could not write to file");
			System.exit(1);
		}
		
		for(int i=0;i<=keywords.size();i++){
			
			if(!keywords.containsValue(i))
				continue;
			
			w = list[i].trim() + "\n";
			
			
			try{
				graphFile.write(w.getBytes());
			}catch(IOException x){
				System.out.println("Could not write to file");
				System.exit(1);
			}
			
		}
		
		try{
			graphFile.close();
		}catch(IOException x){
			System.out.println("Could not close the file");
			System.exit(1);
		}
		
		
	}

	/**
	 * An instance method to assign clusters to keywords in the keyword pool
	 * @param filename	Name of the Metis output file
	 */
	public void assignClustersToKeywords(String filename){
		
		
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
	
		String Line = null;
		String f = filename;
		Keyword k = null;
		
		int count = 1;
		
		try{
			fstream = new FileInputStream(f);
			//System.out.println("fstream done");
			in = new DataInputStream(fstream);
			//System.out.println("data done");
			br = new BufferedReader(new InputStreamReader(in));
			//System.out.println("buffer done");
		}catch(FileNotFoundException x){
			System.out.println("File:" + filename + " could not be opened: ");
			System.exit(1);
		}
		
		try{
				while ((Line = br.readLine()) != null){
					
				
					if(!keywords.containsValue(count)){
						System.out.println("Missed:"+count);
					}else{
					
						Line.replaceAll("\\W","");
					
						k = keywords.getKey(count);
						k.setCluster(Integer.parseInt(Line));
					}
					
					count++;
				}
				
				//System.out.println("Lines:"+count);
				
			}catch(IOException e){
				System.out.println("Problem while assigning cluster");
			}
			
	try{
			fstream.close();
			in.close();
			br.close();
		}catch(IOException e){
			System.out.println("Problem while closing the Datafile");
			//System.exit(1);
		}
		
		
	}
	
	/**
	 * An instance method to retrieve the affinity vector for a given set of keywords (Generally linked to a videoid)
	 * @param words An array of strings which are keywords related to a videoid
	 */
public double[] getAffinityVector(String[] words){
	
	double[] ranking = new double[components];
	String temp = null;
	Keyword k = null;
	int index = 0;
	double freq = 0;
	double total = 0;
	
	
		for(int i=0; i< words.length; i++ ){
		
			
			
			try
			{
				temp = Keyword.purify(words[i]);
			
					if(temp == null)
						continue; 
			
					k = new Keyword(temp,0);
			
					if(!keywords.containsKey(k))
						continue;
			
					index = keywords.get(k);
					k = keywords.getKey(index);
			
					
					//freq = k.getOccurrence()/totalOccurance;
					freq = k.getOccurrence();
					ranking[k.getCluster()] += freq;
			
					total += freq;
			
			}catch(ArrayIndexOutOfBoundsException x){
				continue;
			}
		}
	
	
		for(int i = 0; i < ranking.length ; i++){
			ranking[i] = ranking[i]/total;
		}
	
	return ranking;
	}

}
