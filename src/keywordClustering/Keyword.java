/**
 * Package keywordClustering: Contains all the classes for clustering keywords
 */
package keywordClustering;

/**The Keyword Class: Stores each keyword as a instance
 * 
 * @author Rajat Raychaudhuri
 */
public class Keyword {
	
	/**	The actual word	*/
	private String word;
	
	/**	Degree of the keyword in the keyword Graph	*/
	private int degree;
	
	/**	Checks the occurrence of the keyword in the raw input file; presently not used	*/
	private int occurrence;
	
	/**	The index of the keyword in the pool of all keywords (used by KeywordGraph class)*/
	private int index;
	
	/**	The cluster to which it belongs*/
	private int cluster;
	
	/** A method with restricted access(package) to increase the degree */
	void increaseDegree(){
		degree++;
	}
	
	
	
	/** A method with restricted access(package) to decrease the degree */
	void decreaseDegree(){
		degree--;
	}
	
	/** 
	 * Creates a keyword with the specified parameters
	 * 
	 * @param keyword			The word
	 * @param deg				Degree of the keyword in the keyword graph
	 * @param occurs			The occurrence of the keyword in the raw input file
	 * @param ind				The index of the keyword in the pool of all keywords
	 */
	public Keyword(String keyword,int deg,int occurs,int ind){
		word = keyword;
		degree = deg;
		occurrence = occurs;
		index = ind;
	}
	
	
	/** 
	 * Calls Keyword(String keyword,int deg,int occurs,int ind) with deg = 0 and occurs = 1
	 * 
	 * @param keyword			The word
	 * @param ind				The index of the keyword in the pool of all keywords
	 */
	public Keyword(String keyword,int ind){
		this(keyword,0,1,ind);
	}
	
	
	/** 
	 * Getter method for word with public access
	 * 
	 * @return		the word
	 */
	public String getWord(){
		return word;
	}
	
	/** 
	 * Getter method for degree with public access
	 * 
	 * @return		the degree value of the keyword
	 */
	public int getDegree(){
		return degree;
	}
	
	/** 
	 * Getter method for occurrence with public access
	 * 
	 * @return		the occurrence value of the keyword
	 */
	public int getOccurrence(){
		return occurrence;
	}
	
	/** 
	 * Getter method for index with public access
	 * 
	 * @return		the index value of the keyword
	 */
	public int getIndex(){
		return index;
	}

	/** 
	 *  Public Static method to purify the words. Removes the leading and trailing whitespace and converts to uppercase
	 * 
	 * @param crude				The word to be purified
	 * @return					The modified word
	 */
	public static String purify(String crude){
		String pure = crude;
		String[] temp;
		
		pure.trim();
		pure.replaceAll("^\\s+", "");
		pure.replaceAll("\\s+$", "");
		
		temp = pure.split(" ");
		if(temp.length == 2)
			pure = temp[1];
		else
			pure = temp[0];
		
		return pure.toUpperCase();
	}

	/** 
	 * Setter method for occurrence with restricted access(package).
	 * 
	 * @param occur				number of times the keyword occurs in the raw input file
	 */
	void increaseOccurrence(){
		occurrence++;
	}

	/**
	 * Setter method for index with restricted access(package).
	 * 
	 * @param ind				Index of the keyword in the pool of all keywords
	 */
	void setIndex(int ind){
		index = ind;
	}
	
	/**
	 * @return		The hash code of the word stored in the instance. Hence uses the hashCode method of String
	 */
	public int hashCode(){
		return word.hashCode();
	}

	/**
	 * @param other	the object to be compared with		
	 * @return		returns true if the other object is of type keyword or string and the word exactly matches with the word stored in the instance
	 */
	public boolean equals(Object other){
		
		
		if(other instanceof Keyword){
			Keyword k = (Keyword)other;
			return word.equals(k.word);
		}else if (other instanceof String){
			return word.equals((String)other);
		}else {
			return false;
		}
	}

	
	/** 
	 * Setter method for degree with restricted access(package).
	 * 
	 * @param deg				Degree of the keyword in the keyword graph
	 */
	void setDegree(int deg){
		degree = deg;
	}

	/** 
	 * Setter method for Cluster with restricted access(package).
	 * 
	 * @param cl				cluster to which the keyword belongs
	 */
	void setCluster(int cl){
		cluster = cl;
	}
	
	/** 
	 * Getter method for cluster with public access
	 * 
	 * @return		the cluster to which the keyword belongs
	 */
	public int getCluster(){
		return cluster;
	}

}
