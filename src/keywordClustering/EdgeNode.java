/**
 * 
 */
package keywordClustering;

/**
 * @author rajatraychaudhuri
 *
 */
class EdgeNode {
	
	private int index;
	private int weight;
	
	public EdgeNode(int ind,int w){
		index = ind;
		weight = w;
	}
	
	
	public EdgeNode(int ind){
		this(ind,1);
	}
	
	public int getIndex(){
		return index;
	}
	
	public int getWeight(){
		return weight;
	}
	

}
